package com.blaszczyk.workshop;

import com.blaszczyk.workshop.codewars.Metro;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class MetroTest {
    Metro metro = new Metro();

    @Test
    public void test1() {
        ArrayList<int[]> list = new ArrayList<int[]>();
        list.add(new int[] {10,0});
        list.add(new int[] {3,5});
        list.add(new int[] {2,5});
        assertEquals(5, metro.countPassengers(list));
    }

    @Test
    public void test2() {
        ArrayList<int[]> list = new ArrayList<int[]>();
        list.add(new int[] {10,0});
        list.add(new int[] {3,5});
        list.add(new int[] {2,5});
        list.add(new int[] {4,0});
        assertEquals(9, metro.countPassengers(list));
    }

    @Test
    public void test3() {
        ArrayList<int[]> list = new ArrayList<int[]>();
        list.add(new int[] {2,1});
        list.add(new int[] {5,3});
        list.add(new int[] {0,3});
        list.add(new int[] {18,0});
        list.add(new int[] {2,10});
        assertEquals(10, metro.countPassengers3(list));
    }

}