package com.blaszczyk.workshop;

import com.blaszczyk.workshop.codewars.Fibonacci;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciTest {

    @Test
    public void test() {
        Long except = 5l;
        Long except2 = 8l;
        assertEquals(except, Fibonacci.fib(5));
        assertEquals(except2, Fibonacci.fib(6));
    }

    @Test
    public void test2() {
        Long except = 5l;
        Long except2 = 8l;
        assertEquals(except, Fibonacci.fib(5));
        assertEquals(except2, Fibonacci.fib(6));
    }

}
