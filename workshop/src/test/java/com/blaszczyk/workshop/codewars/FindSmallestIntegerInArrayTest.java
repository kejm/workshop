package com.blaszczyk.workshop.codewars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindSmallestIntegerInArrayTest {

    @Test
    public void example1(){
        int expected = 11;
        int actual = FindSmallestIntegerInArray.findSmallestInt(new int[]{78,56,232,12,11,43});
        assertEquals(expected, actual);
    }


    @Test
    public void example2(){
        int expected = -33;
        int actual = FindSmallestIntegerInArray.findSmallestInt(new int[]{78,56,-2,12,8,-33});
        assertEquals(expected, actual);
    }

    @Test
    public void example3(){
        int expected = Integer.MIN_VALUE;
        int actual = FindSmallestIntegerInArray.findSmallestInt(new int[]{0,Integer.MIN_VALUE,Integer.MAX_VALUE});
        assertEquals(expected, actual);
    }

    @Test
    public void example4(){
        int expected = 11;
        int actual = FindSmallestIntegerInArray.smallestInt(new int[]{78,56,232,12,11,43});
        assertEquals(expected, actual);
    }


    @Test
    public void example5(){
        int expected = -33;
        int actual = FindSmallestIntegerInArray.smallestInt(new int[]{78,56,-2,12,8,-33});
        assertEquals(expected, actual);
    }

    @Test
    public void example6(){
        int expected = Integer.MIN_VALUE;
        int actual = FindSmallestIntegerInArray.smallestInt(new int[]{0,Integer.MIN_VALUE,Integer.MAX_VALUE});
        assertEquals(expected, actual);
    }

}