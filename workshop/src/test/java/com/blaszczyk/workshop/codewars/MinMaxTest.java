package com.blaszczyk.workshop.codewars;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class MinMaxTest {

    @Test
    public void testExampleCases() {
        assertArrayEquals(new int[]{1,5}, MinMax.minMax(new int[]{1,2,3,4,5}));
        assertArrayEquals(new int[]{5, 2334454}, MinMax.minMax(new int[]{2334454,5}));
        assertArrayEquals(new int[]{1, 1}, MinMax.minMax(new int[]{1}));
    }

}