package com.blaszczyk.workshop.codewars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountMonkeysTest {

    @Test
    void monkeyCountTestMethod() {
        assertArrayEquals(new int[]{1,2,3,4,5},CountMonkeys.monkeyCount(5));
    }

}