package com.blaszczyk.workshop;

import com.blaszczyk.workshop.codewars.FunWithString;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunWithStringTest {

    @Test
    public void testRemoval() {
        assertEquals("loquen", FunWithString.stringCutOff("eloquent"));
        assertEquals("ountr", FunWithString.stringCutOff("country"));
    }

}
