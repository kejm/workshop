package com.blaszczyk.workshop;

import com.blaszczyk.workshop.codewars.YesOrNo;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class YesOrNoTest {

    @Test
    public void testBoolToWord() {
        assertEquals(YesOrNo.boolToWord(true),"Yes");
        assertEquals(YesOrNo.boolToWord(false),"No");
    }

    @Test
    public void testBoolToWord0() {
        assertEquals(YesOrNo.boolToWord0(true),"Yes");
        assertEquals(YesOrNo.boolToWord0(false),"No");
    }

}
