package com.blaszczyk.workshop.codewars;

public class Money {

    public static int calculateYears(double principal, double interest,  double tax, double desired) {

        int howYears = 0;
        double moneyAfterYear;

        for( int i = 0; i <= 99; i++) {

            while(principal >= desired) {
                return howYears;
            }

            moneyAfterYear = moneyAfterYear(principal,interest, tax);
            principal = moneyAfterYear;
            howYears ++;

        }

        return howYears;
    }

    public static double moneyAfterYear(double principal, double interest,  double tax) {
        double interestRateOdsetki = principal * interest;
        double taxFromInterestRate = tax * interestRateOdsetki;
        double moneyAfterYear =  principal + (interestRateOdsetki - taxFromInterestRate);
        return moneyAfterYear;
    }

}
