package com.blaszczyk.workshop.codewars;

public class YesOrNo {

    public static String boolToWord(boolean b) {
        if (b == true) {
            String result = "Yes";
            return result;
        }
        return "No";
    }

    public static String boolToWord0(boolean b) {
        return b ? "Yes" : "No";
    }

}
