package com.blaszczyk.workshop.codewars;

class Diamond {

    public static void main (String[] args) {
        System.out.println(print(3));
    }

    public static String print(int n) {

        if(n < 1)
            return null;

        if(ifEven(n) == true)
            return null;

        return drawDiamond(n);
    }

    public static String drawDiamond(int n) {

        StringBuffer diamond = new StringBuffer();
        StringBuffer diamond2 = new StringBuffer();

        for(int i = 1; i <= n; i+=2) {

            for(int k = (n-2); k >= i-1; k-=2) {
                diamond.append(" ");
            }

            for(int j = 0; j <= i-1; j++) {
                diamond.append("*");
            }

            diamond.append("\n");

        }

        for(int i = 2; i <= n; i+=2) {

            for(int k = 2; k <= i; k+=2) {
                diamond2.append(" ");
            }

            for(int j = n; j >= i+1; j--) {
                diamond2.append("*");
            }

            diamond2.append("\n");

        }

        return diamond.append(diamond2).toString();
    }

    public static boolean ifEven(int n){
        if ((n % 2) >= 1){
            return false;
        }
        return true;
    }

}




