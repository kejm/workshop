package com.blaszczyk.workshop.codewars;

import java.util.ArrayList;

public class Metro {

    public static int countPassengers(ArrayList<int[]> stops) {
        int result = 0;
        for(int i = 0; i < stops.size();i++) {
            result += stops.get(i)[0];
            result -= stops.get(i)[1];
        }
        return result;
    }

    public static int countPassengersByStream(ArrayList<int[]> stops){
        return stops.stream()
                .mapToInt(x -> x[0] - x[1])
                .sum();
    }

    public static int countPassengers3(ArrayList<int[]> stops) {
        int result = 0;
        for(int i = 0; i < stops.size();i++) {
            result += stops.get(i)[0] - stops.get(i)[1] ;
        }
        return result;
    }

}
