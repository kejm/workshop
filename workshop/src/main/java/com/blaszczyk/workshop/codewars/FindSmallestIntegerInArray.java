package com.blaszczyk.workshop.codewars;

import java.util.Arrays;

public class FindSmallestIntegerInArray {

    public static void main(String[] args){

        int[] ints = {34, -345, -1, 100};
        System.out.println(findSmallestInt(ints));

    }

    public static int smallestInt(int[] numbers){
        return Arrays.stream(numbers)
                .min()
                .getAsInt();
    }

    public static int findSmallestInt(int[] args) {

        int min = args[0];

        for(int i = 1; i<args.length; i++ ) {
            if(args[i]<min){
                min = args[i];
            }
        }

        return min;

    }

}
