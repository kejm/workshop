package com.blaszczyk.workshop.codewars;

public class SquareSum
{

    public static int squareSum(int[] n)
    {
        int result;
        result = 0;
        for(int i=0; i<n.length; i++) {
         result = (n[i] = n[i]*n[i]) + result;
        }
        return result;
    }

    public static int squareSum1(int[] n)
    {
        int result = 0;

        for(int i=0; i<n.length; i++) {
            n[i] = n[i]*n[i];
            result = n[i] + result;
        }

        return result;
    }
}
