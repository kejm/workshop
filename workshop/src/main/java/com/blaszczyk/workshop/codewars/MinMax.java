package com.blaszczyk.workshop.codewars;

class MinMax {

    public static int[] minMax(int[] arr) {

        int max = arr[0];
        int min = max;

        for(int i = 1; i < arr.length; i++){
            if(arr[i] > arr[i-1]){
                max = arr[i];
            }
            if(arr[i] < arr[i-1]){
                min = arr[i];
            }
        }

        int[] ints = {min,max};
        return ints;
    }

}
