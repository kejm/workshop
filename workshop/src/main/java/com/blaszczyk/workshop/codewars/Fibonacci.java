package com.blaszczyk.workshop.codewars;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Fibonacci {

    public static Long fib(int n){
        LinkedList<Long> list=new LinkedList<Long>();
        list.add(0l);
        list.add(1l);
        for(int i=1; i<=(n-1); i++) {
            list.add((i+1), (list.get(i) + list.get(i-1)));
        }
        return list.get(n);
    }

    public Long fib2(int n){
        Map<Integer, Long> myMap = new HashMap<Integer, Long>();
        myMap.put(0,0l);
        myMap.put(1,1l);
        for(int i=1; i<=n; i++) {
            myMap.put((i+1),(myMap.get(i) + myMap.get(i-1)));
        }
        return myMap.get(n);
    }

}

