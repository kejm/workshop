package com.blaszczyk.workshop.codewars;

import java.util.Collections;

public class StringRepeat {

    public static String repeatStr(final int repeat, final String string) {
        StringBuilder builder = new StringBuilder();
        for(int i=1; i<=repeat; i++) {
            builder.append(string);
        }
        return builder.toString();
    }

    public static String repeatStr2(final int repeat, final String string) {
        String str = "";
        for(int i=1; i<=repeat; i++)
            str += string;
        return str;
    }

    public static String repeatStr3(final int repeat, final String string) {
        return repeat < 0 ? "" : String.join("", Collections.nCopies(repeat, string));
    }

}
