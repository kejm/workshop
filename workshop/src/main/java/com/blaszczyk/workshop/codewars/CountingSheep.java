package com.blaszczyk.workshop.codewars;

public class CountingSheep {

    public int countSheeps(Boolean[] arrayOfSheeps) {
        int numberOfSheep = 0;
        for(int i=0; i<arrayOfSheeps.length; i++) {
            if(arrayOfSheeps[i] == true)
                numberOfSheep++;
        }
        return numberOfSheep;
    }

    public int countSheeps2(Boolean[] arrayOfSheeps) {
        int count = 0;
        for (Boolean b : arrayOfSheeps)
            if (b) count++;
        return count;
    }

}
