package com.blaszczyk.workshop.lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class UseMethodFunctionInterface {

    public static void main(String[] args) {

        // Klasyczne wywołanie metody na obiekcie
        Object objectInstance2 = new Object();
        System.out.println(objectInstance2.hashCode());

        // Wywołanie metody jednej klasy i przypisanie wyniku do obiektu innej klasy
        Object objectInstance = new Object();
        IntSupplier equalsMethodOnObject = objectInstance::hashCode;
        System.out.println(equalsMethodOnObject.getAsInt());

        // Odwołanie się do metody :: bez podania instancji
        ToIntFunction<Object> hashCodeMethodOnClass = Object::hashCode;
        Object objectInstance3 = new Object();
        System.out.println(hashCodeMethodOnClass.applyAsInt(objectInstance3));



        Supplier<Object> objectCreator = Object::new;
        // same as new Object()
        Object objectInstance5 = objectCreator.get();
        System.out.println(objectInstance5);

        BiPredicate<Object, Object> equalsMethodOnClass = Object::equals;
        // same as objectInstace.equals(new Ojbect())
        System.out.println(equalsMethodOnClass.test(objectInstance5, new Object()));

        Predicate<Object> equalsMethodOnObject2 = objectInstance5::equals;
        // same as objectInstace.equals(new Ojbect())
        System.out.println(equalsMethodOnObject2.test(new Object()));



        BiFunction<Integer, String, Human> humanConstructor = Human::new;
        Human human = humanConstructor.apply(25, "Zenek");
        System.out.println(human.getAge() + human.getName());


        BiFunction<String, Integer, Alien> AlienConstructor = Alien::new;
        Alien alien = AlienConstructor.apply("green", 5);
        Alien alien2 = AlienConstructor.apply("dark blue", 23);
        Alien alien3 = AlienConstructor.apply("red", 254);
        List<String> nameOfAliens = Arrays.asList(alien.getColor(), alien2.getColor());
        nameOfAliens
                .stream()
                .map(String::toUpperCase)
                .filter(x -> x.startsWith("D"))
                .forEach(System.out::println);

    }

}
