package com.blaszczyk.workshop.lambda;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Alien {

    private String color;
    private int age;

}
