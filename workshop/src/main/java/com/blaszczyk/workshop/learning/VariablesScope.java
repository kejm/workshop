package com.blaszczyk.workshop.learning;

public class VariablesScope {

    int x = 25;

    public static void main(String[] args) {
        VariablesScope app = new VariablesScope();
        {
            int x = 5;
        }
        {
            int x = 10;
        }
        int x = 100;
        System.out.println(x);
        System.out.println(app.x);
    }

    public VariablesScope() {
        int x = 1;
        System.out.println(x);
    }

}
