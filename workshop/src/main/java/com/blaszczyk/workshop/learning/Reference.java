package com.blaszczyk.workshop.learning;

public class Reference {

    public static void main(String[] args) {

        Reference reference = new Reference();
        Reference reference2 = new Reference();
        Reference reference3 = reference;

        System.out.println(reference.toString());
        System.out.println(reference2.toString());
        System.out.println(reference3.toString());

        System.out.println(reference3==reference);
        System.out.println(reference==reference2);
        System.out.println(reference.equals(reference2));
        System.out.println(reference.equals(reference3));



        Object referenceToObjectX = new Object();
        Object anotherReferenceToObjectX = referenceToObjectX;
        Object refferenceToObjectY = new Object();

        System.out.println(referenceToObjectX.toString());
        System.out.println(anotherReferenceToObjectX.toString());
        System.out.println(refferenceToObjectY.toString());

    }

}
