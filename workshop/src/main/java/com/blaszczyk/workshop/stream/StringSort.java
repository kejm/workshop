package com.blaszczyk.workshop.stream;

import java.util.*;
import java.util.stream.Collectors;

public class StringSort {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println(" Podaj trzy stringi: " );
        String s1 = scanner.next();
        String s2 = scanner.next();
        String s3 = scanner.next();

        List<String> strings = Arrays.asList(s1, s2, s3);

        strings
                .stream()
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList())
                .forEach(x -> System.out.println(x));

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj 4 lancuchy znakow oddzielajac je znakiem nowej linii:");
//        List<String> strings = new ArrayList<>();
//
//        for (int i = 0; i < 4; i++) {
//            strings.add(scanner.nextLine());
//        }
//
//        System.out.println(strings);
//        strings.sort((a, b) -> b.length() - a.length());
//        // strings.sort(Comparator.comparingInt(String::length).reversed());
//        System.out.println(strings);

    }




}
