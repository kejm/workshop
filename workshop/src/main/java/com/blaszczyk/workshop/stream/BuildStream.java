package com.blaszczyk.workshop.stream;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class BuildStream {

    public static void main(String[] args) {

        //Stream na podstawie kolekcji
        Stream<Integer> stream1 = new LinkedList<Integer>().stream();

        //Strumień na podstawie tablicy
        Stream<Integer> stream2 = Arrays.stream(new Integer[]{1,2});

        //Strumien zbudowany na podstawie łańcucha znaków oddzielonego przez wyrażenie regularne
        Stream<String> stream3 = Pattern.compile(".").splitAsStream("some string");
        stream3.forEach(x -> System.out.println(x));

        //Strumienie na podstawie typów prymitywnych
        DoubleStream streamOfDoubles = DoubleStream.of(1, 1.2, 4);
        IntStream ints = IntStream.range(0, 123);
        LongStream longs = LongStream.generate(() -> 1L);

    }

}
