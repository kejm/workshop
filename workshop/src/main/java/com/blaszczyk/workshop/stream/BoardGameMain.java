package com.blaszczyk.workshop.stream;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class BoardGameMain {

    public static void main(String[] args) {

        List<BoardGame> games = Arrays.asList(
                new BoardGame("Terraforming Mars", 8.38, new BigDecimal("123.49"), 1, 5),
                new BoardGame("Codenames", 7.82, new BigDecimal("64.95"), 2, 8),
                new BoardGame("Puerto Rico", 8.07, new BigDecimal("149.99"), 2, 5),
                new BoardGame("Terra Mystica", 8.26, new BigDecimal("252.99"), 2, 5),
                new BoardGame("Scythe", 8.3, new BigDecimal("314.95"), 1, 5),
                new BoardGame("Power Grid", 7.92, new BigDecimal("145"), 2, 6),
                new BoardGame("7 Wonders Duel", 8.15, new BigDecimal("109.95"), 2, 2),
                new BoardGame("Dominion: Intrigue", 7.77, new BigDecimal("159.95"), 2, 4),
                new BoardGame("Patchwork", 7.77, new BigDecimal("75"), 2, 2),
                new BoardGame("The Castles of Burgundy", 8.12, new BigDecimal("129.95"), 2, 4)
        );

        games.stream()
                .filter( x -> x.maxPlayers > 4)
                .filter( x -> x.rating > 8)
                .filter( x -> new BigDecimal(150).compareTo(x.price) > 0)
                .forEach(x -> System.out.println(x.name.toUpperCase()));

        games.stream()
                .filter( x -> x.maxPlayers > 4)
                .filter( x -> x.rating > 8)
                .filter( x -> new BigDecimal(150).compareTo(x.price) > 0)
                .map( x -> x.name.toUpperCase())
                .forEach(System.out::println);

        BoardGame bestGame = BoardGame.GAMES
                .stream()
                .filter( x -> x.name.contains("a"))
                .max(Comparator.comparingDouble( x -> x.rating)).get();
        System.out.println(bestGame.getName());

    }

}
