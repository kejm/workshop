package com.blaszczyk.workshop.stream;

import java.io.IOException;
import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class JavaStreams {

    public static void main(String[] args) throws IOException {

        // 1. Integer Stream
//        IntStream
//                .range(1,10)
//                .forEach(System.out::print);
//        System.out.println();


//        IntStream
//                //w przypadku IntStream generuje w streamie inty od 0 do 9
//                .range(0,10)
//                //metoda skip pomija poczatkowe 5 znaków
//                .skip(5)
//                //metoda or each iteruje po streamie
//                .forEach(x -> System.out.println(x));
//
//
//        //stream integerów, który sumuje wszystkie znaki 1+2+3+4 = 10
//        System.out.println(
//                IntStream.range(1, 5).sum());
//
//        IntStream.range(1,5).forEach(x -> System.out.println(x));
//
//
//        Stream.of("Pola", "kejm", "Marcin")
//                .sorted()
//                .findFirst()
//                .ifPresent(System.out::println);
//
//
//        String[] names = {"Pola", "kejm", "Marcin", "Edward", "Maria"};
//        Arrays.stream(names)
//                .filter(x -> x.startsWith("M"))
//                .sorted()
//                .forEach(System.out::println);
//
//
//        Arrays.stream(new int[] {2,4,6,8, 10})
//                .map(x -> x * x)
//                // Zwrca średnią arytmetyczną elementów streama
//                .average()
//                .ifPresent(System.out::println);
//
//
//        Arrays.stream(new int[] {2,4,6,8, 10})
//                .map(x -> x * x)
//                .forEach(x -> System.out.println(x));
//
//
//          List<String> people = Arrays.asList("Pola", "kejm", "Arek", "Maria");
//          people
//                  .stream()
//                  .map(String::toLowerCase)
//                  .filter( x -> x.startsWith("a"))
//                  .forEach(System.out::println);
//
//
//        double total = Stream.of(7.3, 10.5, 4.8)
//                //dodało wszystkie lementy do siebie i do 0.0
//                .reduce(0.0, (Double a, Double b) -> a + b);
//        System.out.println(" Total = " + total);
//
//
//        IntSummaryStatistics summary = IntStream.of(2, 3, 5, 9, 88, 10)
//                //Wylicza statystyki intów w stream
//                .summaryStatistics();
//        System.out.println(summary);
    }

}
