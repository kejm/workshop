package com.blaszczyk.workshop.mtacertification;

import java.util.Arrays;

public class MtaCertification {
    public static void main(String[] args) {

//        Double number = Double.parseDouble("2321");
//        System.out.println(number);

//        System.out.println(negtiveSquare(4));

//        Double[][] maxArray = {{44.5,23.3,6.7},{37.3,2.3,4.1}};
//        System.out.println(maxArray[0][2]);

//        https://www.dumpsbase.com/freedumps/mta-introduction-to-programming-using-java-98-388-dumps.html?fbclid=IwAR2CFmp-L0gbzdbswRRqBjFZyCzrPSeTNRUs034j1t7Mw3HHMGlVP21zDrg

//        int a = 5;
//        int b = 10;
//        int c = ++a * b--;
//        System.out.println(c);
//        int d = a-- + ++b;
//        System.out.println(d);
//
//         c = 60 d = 16;   operatory postawione z prawej strony oznaczają zmiane wartości w kolejnej operacji, a z lewej w aktualnej.
//        //////////////
//
//        double number = 27;
//        number %= -3d;
//        // wynikiem jest reszta z dzielenia 27 podzielic na -3 to -9 reszty 0 / double jest typem zmiennoprzecinkowym zatem wynik 0.0
//        System.out.println(number);
//        // dodanie do 0.0 liczby 10, f oznacza float? ?
//        number += 10f;
//        System.out.println(number);
//        // 10.0   razy -4  daje   -40.0
//        number *= -4;
//        System.out.println(number);
//
//        ////////////////
//
//        char data1 = 65;
//        System.out.println(data1);
//        long data2 = 65;
//        System.out.println(data2);
//        // Nowy obiekt Float przyjął stringa i zapakował go w zmiennoprzecinkowego prostego floata
//        float data3 = new Float("-65.0");
//        System.out.println(data3);
//        // typ prymitywny short nie jest w stanie wyświetlić liczby przecinkowej, co powoduje błąd z pakietu java.lang
//        short data4 = new Short("65.0");
//        System.out.println(data4);
//
//        ////////////////
//
//        double pi = Math.PI;
//        System.out.format("Pi is %.3f%n", pi);
//        System.out.format("Pi is %.0f%n", pi);
//        System.out.format("Pi is %09f%n", pi);
//
//         3,145;  3;  03,141593;
//        //////////////
//
//        Scanner sc = new Scanner("1 Excellent 2 Good 3 Fair 4 Poor");
//        Object data1 = sc.next();
//        Object data2 = sc.next();
//        Object data3 = sc.nextInt();
//        Object data4 = sc.nextLine();
//        System.out.println(data1);
//        System.out.println(data2);
//        System.out.println(data3);
//        System.out.println(data4);
//
//         1;  Excellent;  2;  Good 3 Fair 4 Poor;
//        ////////////
//
//        DataReader.getBirthdate();
//
//        ////////////
//
//        Main test = new Main();
//        test.printInt();
//
//        /////////////
//
//        System.out.println(Main.fee('A'));
//        System.out.println(Main.fee('T'));
//        System.out.println(Main.fee('C'));
//        System.out.println(Main.fee('D'));
//
//        50; 5; 5; 100;   Ponieważ brak breake sprawi, że price zostanie nadana wartość 5;
//        /////////////
//
//        int num1 = 10;
//        int num2 = 20;
//        int num3 = 30;
//
//        int[] numbers = {num1, num2, num3};
//        System.out.println(numbers[0]);
//        System.out.println(numbers[1]);
//        System.out.println(numbers[2]);
//
//        ////////////
//
//        char[][] grid = {
//                {'-','-','X'},
//                {'-','-','-'},
//                {'-','O','-'}
//        };
//
//        System.out.println(grid[0][2]);
//        System.out.println(grid[2][1]);
//
//        ////////////
//
//        double dNum = 2.667;
//        int iNum = 0;
//        iNum = (int)dNum;
//
//        System.out.println(Double.parseDouble("23.4"));
//
//        String welcomeMsg = "Welcome, ";
//        String firstName = "Pola";
//        welcomeMsg += firstName.substring(0,1).toUpperCase() + firstName.substring(1).toLowerCase();
//        System.out.println(welcomeMsg);
//
//
//       ////////////
//
//    }
//
//    public void printInt() {
//        if (true) {
//            int num = 1;
//            if (num > 0) {
//                num++;
//            }
//        }
//        int num = 1;
//        addOne(num);
//        num = num -1;
//        System.out.println(num);
//    }
//
    }


//    public void addOne(int num) {
//        num = num + 1;
//    }
//
//    public static int fee(char model) {
//        int price = 0;
//        switch (model) {
//            case 'A':
//                price = 50;
//                break;
//            case 'T':
//                price = 20;
//            case 'C':
//                price = 5;
//                break;
//            default:
//                price = 100;
//                break;
//        }
//        return price;
//    }
//
//    public static int negtiveSquare(int number) {
//        return -(number*number);
//    }

}
