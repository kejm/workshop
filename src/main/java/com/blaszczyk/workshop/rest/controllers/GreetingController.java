package com.blaszczyk.workshop.rest.controllers;

import com.blaszczyk.workshop.rest.services.GreetingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private final GreetingService service;

    public GreetingController(GreetingService service) {
        this.service = service;
    }

    @RequestMapping("/greeting")
    public @ResponseBody String greeting() {
        return service.greet();
    }

    @RequestMapping("/add-result")
    public @ResponseBody Integer addResult() {
        return service.addResult();
    }

}
