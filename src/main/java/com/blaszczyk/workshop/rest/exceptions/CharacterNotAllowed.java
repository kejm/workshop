package com.blaszczyk.workshop.rest.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CharacterNotAllowed extends RuntimeException{

    private static final Logger LOGGER = LogManager.getLogger(CharacterNotAllowed.class.getName());

    public CharacterNotAllowed(String message) {
        LOGGER.info("Invalid character! Please use only 0-9 character");
    }

}
