package com.blaszczyk.workshop.rest.services;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {

    public String greet() {
        return "Hello World";
    }

    public Integer addResult() {
        return 2+3;
    }

}
