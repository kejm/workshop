package com.blaszczyk.workshop;

import com.blaszczyk.workshop.works.example.HowNumbersInString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

public class WorkApp {

    private static final Logger LOGGER = LogManager.getLogger(WorkApp.class.getName());

    public static void main(String[] args) {

        HowNumbersInString howNumbersInString = new HowNumbersInString();
        HashMap actualHashMap = howNumbersInString.characterCount("y34g743f+_");

        System.out.println(actualHashMap.size());

        LOGGER.info("App still run even after ");

    }
}