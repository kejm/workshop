package com.blaszczyk.workshop.works.prime;

import java.util.LinkedList;
import java.util.List;

public class PrimeNumbersGenerator {

    public List generateListOfPrimeNumbers(int number) {
        List list = new LinkedList();

        for(int i=2; i<=number; i++) {
            Boolean ifPrime = CheckIfPrimeNumber.checkIfPrimeNumber(i);

            if(ifPrime==true){
                list.add(i);
            }
        }
        return list;
    }

}
