package com.blaszczyk.workshop.works.prime;

/**
 * Class used to check integer. We can check if the number is prime number
 * prime number is    >1 /number & 1
 * @author Kamil B.
 */
public class CheckIfPrimeNumber {

    public static boolean checkIfPrimeNumber(int number) {

        boolean ifPrime;
        int ifDivide;

        for(int i=2; i<(number-1); i++) {
            ifDivide = number % i;

            if(ifDivide < 1) {
                ifPrime = false;
                return ifPrime;
            }
        }
        ifPrime = true;

        return ifPrime;
    }

}
