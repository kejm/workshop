package com.blaszczyk.workshop.works.even;

public class CheckNumber {

    public static boolean checkIfEven(int number) {
        boolean check = false;
        int c = number  % 2;

        if(c==1) {
            check = false;
        }
        else{
            check = true;
        }
        return check;
    }

}
