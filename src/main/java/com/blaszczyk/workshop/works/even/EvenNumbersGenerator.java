package com.blaszczyk.workshop.works.even;

import java.util.ArrayList;
import java.util.List;

public class EvenNumbersGenerator {

    public List listOfEvenNumbers(int numbers) {
        List<Integer> list = new ArrayList<>();

        for(int i=1; i<numbers; i++) {

            boolean ifEven = CheckNumber.checkIfEven(i);

            if(ifEven==true) {
                list.add(i);
            }
        }
        return list;
    }

}
