package com.blaszczyk.workshop.works;

import java.util.LinkedList;
import java.util.List;

public class FibonacciGenerator {

    //For Long, max Argument is 92
    public List<Long> fibonacciListGenerator(int numberOfCycle) {

        List<Long> listFibonacci = new LinkedList<>();
        listFibonacci.add(0, 1l);
        listFibonacci.add(1, 1l);

        for(int i=2; i<numberOfCycle; i++) {
            listFibonacci.add(i, listFibonacci.get(i-1) + listFibonacci.get(i-2));
        }
        return listFibonacci;
    }

}
