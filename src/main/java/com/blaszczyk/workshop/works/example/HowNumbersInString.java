package com.blaszczyk.workshop.works.example;

import com.blaszczyk.workshop.rest.exceptions.CharacterNotAllowed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HowNumbersInString {

    private static final Logger LOGGER = LogManager.getLogger(HowNumbersInString.class.getName());

    public HashMap<String, Integer> characterCount(String inputString) {

        String myNumbers;
        String myLetter;

        myNumbers = inputString.replaceAll("[^0-9]+", "");
        myLetter = inputString.replaceAll("[^a-z]+", " ");

        List<String> numbersList = Arrays.asList(myNumbers.trim().split(""));
        List<String> lettersList = Arrays.asList(myLetter.trim().split(" "));

        for(int i=0; i < lettersList.size(); i++) {
            LOGGER.fatal(lettersList.get(i));
        }

        HashMap<String, Integer> counterMap = new HashMap<>();

        for(int i=0; i < numbersList.size(); i++) {
            if (counterMap.containsKey(numbersList.get(i))) {
                counterMap.put(numbersList.get(i), counterMap.get(numbersList.get(i)) +1);
            }
            else {
                counterMap.put(numbersList.get(i), 1);
            }
        }

        try{
            checkSpecialCharacter(inputString);
        }
        catch (CharacterNotAllowed e){
            e.printStackTrace();
        }
        finally {
            return counterMap;
        }

    }

    public static boolean checkSpecialCharacter(String str)
    {
        //Budowa instancji klasy Pattern i użycie metody compile, która kompiluje podane wyrażenie w pattern (wzór)
        Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
        //Buduje element dopasowujący, który dopasowuje dane wejściowe do ego wzorca
        Matcher matcher = pattern.matcher(str);
        //Resetuje dopasowanie i szuka kolejnej podsekwencji pasującej do wzorca
        boolean isSpec = matcher.find();

        if (isSpec == true) {
            System.out.println("special character is found ");
            throw new CharacterNotAllowed("error message");
        }
        else {
            System.out.println( "special character is not found");
            return true;
        }
    }

}
