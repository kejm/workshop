package com.blaszczyk.workshop.works;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SimpleMath {

    int a;
    int b;
    int c;

    public static int add(int a, int b) {
        return a+b;
    }

    public static int thirdDegreePower(int c) {
        return c*c*c;
    }

    public static void sortNatural()
    {
        List<Integer> list = new ArrayList<>();
        list.add(0,15);
        list.add(1,5);
        list.add(2,10);
        list.add(2,20);

        list.stream().sorted().forEach(System.out::println);
    }

}
