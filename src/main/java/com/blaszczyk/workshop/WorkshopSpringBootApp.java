package com.blaszczyk.workshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//  @SpringBootApplication - wygodna adnotacja zawierajaca kilka innych:
//  • @Configuration - Wskazuje Springowi na klasę, w której są metody do budowy beanów
//  • @EnableAutoConfiguration - Mówi springowi by zaczał dodawać beany, ktore znajdzie bazujac na ustawieniach, innych beanach.
//  • @EnableWebMvc - jest dodawana automatycznie jeśli (wystepuje spring-webmvc on the classpath) To oznacza apke jako alikacje webowa i aktywuje zachowania takie jak konfiguracja DispatcherServlet
//  • @ComponentScan - Informuje Springa by szukał innych komponentow, konfiguracji i usług takich jak controller.


//@Configuration
//@ComponentScan
//@EnableAutoConfiguration
//@EnableWebMvc
@SpringBootApplication
public class WorkshopSpringBootApp {

    public static void main(String[] args) {
        SpringApplication.run(WorkshopSpringBootApp.class, args);
    }

}
