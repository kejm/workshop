package com.blaszczyk.workshop.sandbox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MocksTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void vMockAnInterface() {
        //Given
        List mockedList = mock(List.class);

        //When
        //obiekt tylko przyjmuje dane
        mockedList.add("one");
        mockedList.clear();

        //Then
        //weryfikacja, czy obiekt przyjal dane
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }

    @Test
    public void mockAndSubbingClass() {
        //You can mock classes, not just interfaces
        LinkedList mockedList = mock(LinkedList.class);

        //When
        //Obiekt przyjmujedane i zwraca dane
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenReturn(new RuntimeException());

        //Then
        //following prints "first"
        System.out.println(mockedList.get(0));

        //following throws runtime exception
        System.out.println(mockedList.get(1));

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));
    }

    @Test
    public void stubbingWithArgumentMatcher() {
        //Mocking class
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("some element");

        //following prints "element"
        System.out.println(mockedList.get(999));

        //you can also verify using an argument matcher
        verify(mockedList).get(anyInt());
        verify(mockedList).get(999);

//        verify(mockedList).someMethod(anyInt(), anyString(), eq("third argument"));

//        More examples:
//        https://javadoc.io/static/org.mockito/mockito-core/3.2.4/org/mockito/ArgumentMatchers.html
    }

    @Test
    public void verifyingExactNumberOfInvocations() {
        LinkedList mockedList = mock(LinkedList.class);

        //using mock
        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

        //exact number of invocations verification
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockedList, never()).add("never happend before");

        //verification using atLeast()/atMost()
        verify(mockedList, atMostOnce()).add("once");
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(1)).add("three times");
        verify(mockedList, atMost(5)).add("three times");
    }

    @Test
    public void verificationInOrder() {
        // A. Single mock whose methods must be invoked in a particular order
        List singleMock = mock(List.class);

        //Using a single mock
        singleMock.add("was added first");
        singleMock.add("was added second");

        //create an inOrder verifier for a single mock
        InOrder inOrder = inOrder(singleMock);

        //following will make sure that add is first called with "was added first", then with "was added second"
        inOrder.verify(singleMock).add("was added first");
        inOrder.verify(singleMock).add("was added second");



        // B. Multiple mocks that must be used in a particular order, but declaration of mock can by random
        List secondMock = mock(List.class);
        List firstMock = mock(List.class);

        //using mocks must by in order
        firstMock.add("was called first");
        secondMock.add("was called second");

        //create inOrder object passing any mocks that need to be verified in order
        InOrder inOrderMultipleMocks = inOrder(firstMock, secondMock);

        //following will make sure that firstMock was called before secondMock
        inOrderMultipleMocks.verify(firstMock).add("was called first");
        inOrderMultipleMocks.verify(secondMock).add("was called second");
    }

    @Test
    public void sureInteractionNeverHappened() {
        List mockOne = mock(List.class);
        List mockTwo = mock(List.class);
        List mockthree = mock(List.class);

        //using mocks - only mockOne is interacted
        mockOne.add("one");

        //ordinary verification
        verify(mockOne).add("one");

        //verify that method was never called on a mock
        verify(mockOne, never()).add("never used two");

        //verify that other mocks were not interacted
        verifyNoInteractions(mockTwo, mockthree);
        verifyNoMoreInteractions(mockOne);
    }

    @Test
    public void findingRendundandInvocations() {
        //create mock
        LinkedList firstMockList = mock(LinkedList.class);

        //add interactions for mock
        firstMockList.add("string 1");
        firstMockList.add("string 2");
        firstMockList.add("string 2");

        //verify all interactions from above
        verify(firstMockList).add("string 1");
        verify(firstMockList, times(2)).add("string 2");

        //Check interactions that never been usen before
        verify(firstMockList, never()).add("string 3");

        //Check did exists another interactions without verify
        verifyNoMoreInteractions(firstMockList);
    }

    @Test
    public void whenCreateMock_thenCreated() {
        List mockedList = Mockito.mock(ArrayList.class);

        mockedList.add("one");

        assertEquals(0, mockedList.size());
        verify(mockedList, times(1)).add("one");
    }

    @Test
    public void whenCreateSpy_thenCreate() {
        List spyList = Mockito.spy(new ArrayList());

        spyList.add("one");
        Mockito.verify(spyList).add("one");

        assertEquals(1, spyList.size());
    }

}
