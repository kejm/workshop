package com.blaszczyk.workshop.sandbox.other;

import com.blaszczyk.workshop.works.SimpleMath;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class AssertsSimpleMatchTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void didReturnIsNotNul() {
        //Given
        int a = 75;
        int b = 25;

        //When
        int solution = SimpleMath.add(a,b);

        //Then
        assertThat(solution).isNotNull();
    }

    @Test
    public void didReturnIsEqualExcepted() {
        //Given
        int a = 75;
        int b = 25;

        //When
        int solution = SimpleMath.add(a,b);

        //Then
        assertThat(solution).isEqualTo(100);
    }

    @Test
    public void didReturnValueIsBetween() {
        //Given
        int a = 75;
        int b = 25;

        //When
        int solution = SimpleMath.add(a,b);

        //Then
        assertThat(solution).isStrictlyBetween(99, 101);
    }

    @Test
    public void mockTest() {
        //Given
        int a = 75;
        int b = 25;

        //When
        SimpleMath simpleMath = mock(SimpleMath.class);

        //Then
        assertThat(simpleMath.add(a, b)).isEqualTo(100);

    }

}
