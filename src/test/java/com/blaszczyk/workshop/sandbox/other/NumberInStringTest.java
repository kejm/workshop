package com.blaszczyk.workshop.sandbox.other;

import com.blaszczyk.workshop.works.example.HowNumbersInString;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class NumberInStringTest {

    @Spy
    private HowNumbersInString howNumbersInString;

    @Test
    public void mapTestWithSpy() {
        //Given
        HashMap<String, Integer> expectHashMap = new HashMap<>();
        expectHashMap.put("7", 1);
        expectHashMap.put("6", 2);
        expectHashMap.put("1", 1);

        HashMap actualHashMap = howNumbersInString.characterCount("y667u1");

        //When
        when(actualHashMap).thenReturn(expectHashMap);

        //Then
        assertEquals(actualHashMap, expectHashMap);
    }

    @Test
    public void  withNotAllowedCharactersTest() {
        //Given
        HashMap<String, Integer> expectHashMap = new HashMap<>();
        expectHashMap.put("8", 2);
        expectHashMap.put("5", 1);

        HowNumbersInString howNumbersInString = new HowNumbersInString();
        HashMap<String, Integer> actualHashMap = howNumbersInString.characterCount("++dd8d++d5d8");

        //Then
        assertTrue(actualHashMap.equals(expectHashMap));
    }

    @Test
    public void emptyMethodArgumentTest() {
        //When
        HowNumbersInString howNumbersInString = spy(HowNumbersInString.class);
        HashMap<String, Integer> actualMap = howNumbersInString.characterCount("");

        //Then
        assertThat(actualMap.size(), is(equalTo(1)));
    }

    @Test
    public void mapContainingTest() {
        //When
        HowNumbersInString howNumbersInString = spy(HowNumbersInString.class);
        HashMap<String, Integer> actualMap = howNumbersInString.characterCount("gg6ff8");

        //Then
        assertThat(actualMap, IsMapContaining.hasKey("6"));
        assertThat(actualMap, IsMapContaining.hasKey("8"));
        assertThat(actualMap, IsMapContaining.hasValue(1));
        verify(howNumbersInString, times(1)).characterCount(anyString());
    }

    @Test
    public void inputOutputTests() {
        //Given
        HowNumbersInString howNumbersInString = mock(HowNumbersInString.class);
        HashMap<String, Integer> actualMap = howNumbersInString.characterCount(anyString());

        //Then
        when(howNumbersInString.characterCount(anyString())).thenReturn(new HashMap<String, Integer>());
        verify(howNumbersInString).characterCount(anyString());
    }

    @Test
    public void standardMapTest() {
        //Given
        HashMap<String, Integer> expectHashMap = new HashMap<>();
        expectHashMap.put("3", 2);
        expectHashMap.put("8", 5);
        expectHashMap.put("5", 5);
        expectHashMap.put("7", 5);

        //When
        HowNumbersInString howNumbersInString = new HowNumbersInString();
        HashMap<String, Integer> actualHashMap = howNumbersInString.characterCount("jd3n8s5a8n3777g8jsngm8gnds5nj585577hhdn");

        //Then
        assertTrue(actualHashMap.equals(expectHashMap));
    }

    @Test
    public void standardMapTest2() {
        //Given
        HashMap<String, Integer> expectHashMap = new HashMap<>();
        expectHashMap.put("7", 1);
        expectHashMap.put("6", 2);
        expectHashMap.put("1", 1);

        HowNumbersInString howNumbersInString = new HowNumbersInString();
        HashMap<String, Integer> actualHashMap = howNumbersInString.characterCount("y667u1");

        //Then
        assertTrue(actualHashMap.equals(expectHashMap));
    }
}
