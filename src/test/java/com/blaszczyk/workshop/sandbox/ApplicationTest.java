package com.blaszczyk.workshop.sandbox;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//  W tym teście uruchamiany jest pełny kontekst aplikacji Spring,ale bez serwera, ktory wymaga zwiększonych zasobów.
//  Możemy zawęzić testy tylko do warstwy internetowej, używając @WebMvcTest
// Adnotacja @SpringBootTest szuka głównej klasy konfiguracyjnej (@Configure, tej z adnotacją SpringBootApplication) i uruchamia kontekst
// SpringRunner to alias do klasy SpringJUnit4ClassRunner,
// @RunWith(SpringRunner.class) - Dołącza do testu możliwość korzystania z biblioteki JUnit dla testów jednostkowych i integracyjnych
// with the Spring TestContext Framework.

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello World")));
    }
}
