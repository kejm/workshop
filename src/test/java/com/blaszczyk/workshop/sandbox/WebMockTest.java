package com.blaszczyk.workshop.sandbox;

import com.blaszczyk.workshop.rest.controllers.GreetingController;
import com.blaszczyk.workshop.rest.services.GreetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// We use @MockBean to create and inject mock for the GreetingService
// (if you don’t do this the application context cannot start),
// Expectations was using by Mockito

@RunWith(SpringRunner.class)
@WebMvcTest(GreetingController.class)
public class WebMockTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GreetingService service;

    @Test
    public void greetingShouldReturnMessageFromService() throws Exception {
        when(service.greet()).thenReturn("Hello Mock");
        this.mockMvc
                .perform(get("/greeting")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello Mock")));
    }

    @Test
    public void addResultShouldReturnMessagefromService() throws Exception {
        when(service.addResult()).thenReturn(5);
        this.mockMvc
                .perform(get("/add-result")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("5")));
    }
    
}
