package com.blaszczyk.workshop.sandbox;

import com.blaszczyk.workshop.rest.controllers.HomeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

// @Autowired is interpreted by the Spring and the controller is injected before the test methods are run.
// We are using AssertJ (assertThat() etc.) to express the test assertions.

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmokeTest {

    @Autowired
    private HomeController controller;

    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }
}


// Tips
// If you have multiple methods in a test case, or multiple test cases with the same configuration,
// they only incur the cost of starting the application once.
// You can control the cache using the @DirtiesContext annotation