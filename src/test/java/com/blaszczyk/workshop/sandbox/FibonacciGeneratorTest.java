package com.blaszczyk.workshop.sandbox;

import com.blaszczyk.workshop.works.FibonacciGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

//@Slf4j
@RunWith(MockitoJUnitRunner.Silent.class)
public class FibonacciGeneratorTest {

    private static final Logger LOGGER = LogManager.getLogger(FibonacciGenerator.class.getName());

    private Long startTime;
    private Long endTime;

    @Mock
    private FibonacciGenerator mockFibonacciGenerator;

    @Spy
    private FibonacciGenerator spyFibonacciGenerator;

    @Before
    public void setUp() {
        startTime = System.currentTimeMillis();
    }

    @After
    public void tearDown() {
        endTime = System.currentTimeMillis();
        LOGGER.info("\n" + "Execution Time = " + (((endTime - startTime )/1000)/60) + "m " + (((endTime - startTime )/1000)%60) + "s "  + (endTime - startTime) + "ms");
    }

    @Test
    public void comparisonActualWithExpectMock() {
        //Given
        List expectList = new LinkedList<>();
        expectList.add(1);
        expectList.add(1);
        expectList.add(2);
        expectList.add(3);
        expectList.add(5);

        List actualList = mockFibonacciGenerator.fibonacciListGenerator(6);
        actualList.add(1);
        actualList.add(1);
        actualList.add(2);
        actualList.add(3);
        actualList.add(5);

        //When
        when(mockFibonacciGenerator.fibonacciListGenerator(6)).thenReturn(expectList);

        //Then
        verify(mockFibonacciGenerator, times(1)).fibonacciListGenerator(6);
        assertEquals(expectList.get(0), actualList.get(0));
        assertEquals(expectList.get(1), actualList.get(1));
        assertEquals(expectList.get(2), actualList.get(2));
        assertEquals(expectList.get(3), actualList.get(3));
    }

    @Test
    public void comparisonActualWithExpectSpy() {
        //Given
        List<Long> actualList = spyFibonacciGenerator.fibonacciListGenerator(6);

        List<Long> expectList = new LinkedList();
        expectList.add(1l);
        expectList.add(1l);
        expectList.add(2l);

        //When
        when(spyFibonacciGenerator.fibonacciListGenerator(6)).thenReturn(expectList);

        //Then
        verify(spyFibonacciGenerator).fibonacciListGenerator(6);
        assertEquals(expectList.get(0), actualList.get(0));
        assertEquals(expectList.get(1), actualList.get(1));
        assertEquals(expectList.get(2), actualList.get(2));
    }

}
