package com.blaszczyk.workshop.sandbox.works;

import com.blaszczyk.workshop.works.prime.PrimeNumbersGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@Slf4j
@RunWith(MockitoJUnitRunner.Silent.class)
public class PrimeNumberGeneratorTest {

    private Long startTime;
    private Long endTime;

    @Spy
    private PrimeNumbersGenerator primeNumbersGenerator;

    @Before
    public void setUp() {
        startTime = System.currentTimeMillis();
    }

    @After
    public void tearDown() {
        endTime = System.currentTimeMillis();
//        log.info("\n" + "Execution Time = " + (((endTime - startTime )/1000)/60) + "m " + (((endTime - startTime )/1000)%60) + "s "  + (endTime - startTime) + "ms");
    }

    @Test
    public void exampleOfUse() {
        //When
        PrimeNumbersGenerator primeNumbersGenerator = new PrimeNumbersGenerator();
        List list = primeNumbersGenerator.generateListOfPrimeNumbers(150000);

        //Then
        assertNotNull(primeNumbersGenerator);
        assertEquals(13848, list.size());
        assertFalse(list.isEmpty());

        if(list.size() !=13848){
            Assert.fail("Something wrong");
        }
    }

    @Test
    public void equalsExpectWithActualWithSpy() {
        //Given
        List expectedPrimeNumbersList = new LinkedList();
        expectedPrimeNumbersList.add(2);
        expectedPrimeNumbersList.add(3);
        expectedPrimeNumbersList.add(5);

        //When
        when(primeNumbersGenerator.generateListOfPrimeNumbers(20)).thenReturn(expectedPrimeNumbersList);
        List actualPrimeNumbersList = primeNumbersGenerator.generateListOfPrimeNumbers(20);

        //Then
        assertEquals(expectedPrimeNumbersList.get(0), actualPrimeNumbersList.get(0));
        assertEquals(expectedPrimeNumbersList.get(1), actualPrimeNumbersList.get(1));
        assertEquals(expectedPrimeNumbersList.get(2), actualPrimeNumbersList.get(2));
    }

    @Test
    public void equalsExpectWithActualWithMock() {
        //Given
        PrimeNumbersGenerator mockPrimeNumbersGenerator = mock(PrimeNumbersGenerator.class);

        List expectedPrimeNumbersList = new LinkedList();
        expectedPrimeNumbersList.add(2);
        expectedPrimeNumbersList.add(3);
        expectedPrimeNumbersList.add(5);

        //When
        List actualPrimeNumbersList = mockPrimeNumbersGenerator.generateListOfPrimeNumbers(150);
        actualPrimeNumbersList.add(2);
        actualPrimeNumbersList.add(3);
        actualPrimeNumbersList.add(5);

        when(mockPrimeNumbersGenerator.generateListOfPrimeNumbers(150)).thenReturn(expectedPrimeNumbersList);

        //Then
        verify(mockPrimeNumbersGenerator, times(1)).generateListOfPrimeNumbers(150);
        assertEquals(expectedPrimeNumbersList.get(0), actualPrimeNumbersList.get(0));
        assertEquals(expectedPrimeNumbersList.get(1), actualPrimeNumbersList.get(1));
        assertEquals(expectedPrimeNumbersList.get(2), actualPrimeNumbersList.get(2));
    }

    @Test
    public void equalsExpectWithActualWithMock2() {
        //Given
        PrimeNumbersGenerator mockPrimeNumbersGenerator = mock(PrimeNumbersGenerator.class);

        List expectedPrimeNumbersList = new LinkedList();
        expectedPrimeNumbersList.add(2);
        expectedPrimeNumbersList.add(3);
        expectedPrimeNumbersList.add(5);

        //When
        when(mockPrimeNumbersGenerator.generateListOfPrimeNumbers(anyInt())).thenReturn(expectedPrimeNumbersList);

        List actualPrimeNumbersList = mockPrimeNumbersGenerator.generateListOfPrimeNumbers(20);

        //Then
        assertEquals(expectedPrimeNumbersList.get(0), actualPrimeNumbersList.get(0));
        assertEquals(expectedPrimeNumbersList.get(1), actualPrimeNumbersList.get(1));
        assertEquals(expectedPrimeNumbersList.get(2), actualPrimeNumbersList.get(2));
    }

    @Test
    public void equalsExpectWithActualWithoutMock() {
        //Given
        PrimeNumbersGenerator primeNumbersGenerator = new PrimeNumbersGenerator();

        List expectedPrimeNumbersList = new LinkedList();
        expectedPrimeNumbersList.add(2);
        expectedPrimeNumbersList.add(3);
        expectedPrimeNumbersList.add(5);

        //When
        List actualPrimeNumbersList = primeNumbersGenerator.generateListOfPrimeNumbers(20);

        //Then
        assertEquals(expectedPrimeNumbersList.get(0), actualPrimeNumbersList.get(0));
        assertEquals(expectedPrimeNumbersList.get(1), actualPrimeNumbersList.get(1));
        assertEquals(expectedPrimeNumbersList.get(2), actualPrimeNumbersList.get(2));
    }
}
