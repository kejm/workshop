package com.blaszczyk.workshop.sandbox.works;

import com.blaszczyk.workshop.works.even.EvenNumbersGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EvenNumberGeneratorTest {

    @Mock
    private EvenNumbersGenerator evenNumbersGenerator;

    @Test
    public void equalsExpectedWithActualSpy() {
        //Given
        EvenNumbersGenerator evenNumbersGenerator = spy(EvenNumbersGenerator.class);

        List expectedValuesList = new LinkedList();
        expectedValuesList.add(2);
        expectedValuesList.add(4);
        expectedValuesList.add(6);
        expectedValuesList.add(8);

        //When
        List<Integer> actualValuesList = evenNumbersGenerator.listOfEvenNumbers(10);
        when(evenNumbersGenerator.listOfEvenNumbers(10)).thenReturn(expectedValuesList);

        //Then
        assertEquals(expectedValuesList.get(0), actualValuesList.get(0));
        assertEquals(expectedValuesList.get(1), actualValuesList.get(1));
    }

    @Test
    public void equalsExpectedWithActualMock() {
        //Given
        List expectedValuesList = new LinkedList();
        expectedValuesList.add(2);
        expectedValuesList.add(4);

        //When
        List<Integer> actualValuesList = evenNumbersGenerator.listOfEvenNumbers(anyInt());
        actualValuesList.add(2);
        actualValuesList.add(4);

        when(evenNumbersGenerator.listOfEvenNumbers(anyInt())).thenReturn(expectedValuesList);

        //Then
        assertEquals(expectedValuesList.get(0), actualValuesList.get(0));
        assertEquals(expectedValuesList.get(1), actualValuesList.get(1));
    }

    @Test
    public void equalsExpectedWithActualWithoutMock() {
        //Given
        EvenNumbersGenerator evenNumbersGenerator = new EvenNumbersGenerator();
        List expectedValuesList = new LinkedList();
        expectedValuesList.add(2);
        expectedValuesList.add(4);
        expectedValuesList.add(6);
        expectedValuesList.add(8);

        //Then
        List actualValuesList = evenNumbersGenerator.listOfEvenNumbers(50);
        assertEquals(expectedValuesList.get(0), actualValuesList.get(0));
        assertEquals(expectedValuesList.get(1), actualValuesList.get(1));
    }

    @Test
    public void fiveExamplesOfUseTest() {
        //make interaction
        evenNumbersGenerator.listOfEvenNumbers(10);
        evenNumbersGenerator.listOfEvenNumbers(20);
        evenNumbersGenerator.listOfEvenNumbers(30);
        evenNumbersGenerator.listOfEvenNumbers(40);
        evenNumbersGenerator.listOfEvenNumbers(50);

        //Conditions
        when(evenNumbersGenerator.listOfEvenNumbers(anyInt())).thenReturn(new LinkedList());

        //verification uses interactions
        verify(evenNumbersGenerator, times(5)).listOfEvenNumbers(anyInt());
    }
}
