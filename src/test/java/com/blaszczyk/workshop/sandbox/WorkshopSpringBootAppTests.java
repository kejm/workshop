package com.blaszczyk.workshop.sandbox;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// Test po==uruchamiający sam kontekst Springa, z pustą metodą

@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkshopSpringBootAppTests {

    @Test
    public void contextLoads() throws Exception{
    }

}
